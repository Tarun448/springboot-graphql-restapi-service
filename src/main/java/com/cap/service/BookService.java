package com.cap.service;

import java.util.List;

import com.cap.model.Book;

public interface BookService {
	
	public Book create(Book book);
	
	public List<Book> getAll();
	
	public Book getById(int id);

}
