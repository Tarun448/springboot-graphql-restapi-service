package com.cap.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cap.model.Book;
import com.cap.repository.BookRepository;
import com.cap.service.BookService;


@Service
public class BookServiceImpl implements BookService {
	
	private BookRepository repo;
	
	@Autowired
	public BookServiceImpl(BookRepository repo) {
		this.repo = repo;
	}

	

	@Override
	public Book create(Book book) {
		
		return repo.save(book);
	}

	@Override
	public List<Book> getAll() {
		
		return repo.findAll();
	}

	@Override
	public Book getById(int id) {
		
		return repo.findById(id).get();
	}

}
