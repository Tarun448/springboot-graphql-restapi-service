package com.cap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.cap.model.Book;
import com.cap.service.BookService;

@RestController
@RequestMapping("v1/api")
public class BookController {

	private BookService service;

	@Autowired
	public BookController(BookService service) {
		this.service = service;
	} 
	
	
	@PostMapping("/book")
	public ResponseEntity<Book> create(@RequestBody Book book)
	{
		return new ResponseEntity<Book>(service.create(book),HttpStatus.CREATED);
	}
	
	
	@GetMapping("/books")
	public ResponseEntity<List<Book>> getAllBook()
	{
		return ResponseEntity.ok(service.getAll());
	}
	
	@GetMapping("book/{id}")
	public ResponseEntity<Book> getBookById(@PathVariable("id") int id)
	{
		return ResponseEntity.ok(service.getById(id));
	}
	
	
	
}
