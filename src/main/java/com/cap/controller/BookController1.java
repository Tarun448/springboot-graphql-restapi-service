package com.cap.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import com.cap.model.Book;
import com.cap.model.BookInput;
import com.cap.service.BookService;

@Controller
public class BookController1 {
	
	@Autowired
	private BookService service;
	
	
	@MutationMapping("createBook")
	public Book create(@Argument BookInput book)
	{
		Book b = new Book();
		b.setTitle(book.getTitle());
		b.setDesc(book.getDesc());
		b.setAuthor(book.getAuthor());
		b.setPrice(book.getPrice());
		b.setPages(book.getPages());
		
		return service.create(b);
	}
	
	
	@QueryMapping("allBooks")
	public List<Book> getAllBook()
	{
		return service.getAll();
	}
	
	@QueryMapping("getBook")
	public Book getBookById(@Argument int id)
	{
		return service.getById(id);
	}

}
