package com.cap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.cap.model.Book;
import com.cap.service.BookService;

@SpringBootApplication
public class SpringbootGraphqlApplication implements CommandLineRunner{

	@Autowired
	private BookService service;
	
	public static void main(String[] args) {
		SpringApplication.run(SpringbootGraphqlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

	}

}
