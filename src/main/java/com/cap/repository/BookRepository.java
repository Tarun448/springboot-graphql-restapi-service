package com.cap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cap.model.Book;


@Repository
public interface BookRepository extends JpaRepository<Book, Integer> {

}
